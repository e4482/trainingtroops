<?php
// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Config options generator
 *
 * @package     local_trainingtroops
 * @author      Charly Piva
 * @copyright   (C) Charly Piva 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


// Ensure the configurations for this site are set
/*if ( $hassiteconfig ){
 
	// Create the new settings page
	// - in a local plugin this is not defined as standard, so normal $settings->methods will throw an error as
	// $settings will be NULL
	$settings = new admin_settingpage( 'local_trainingtroops', 'Trainingtroops' );
 
	// Create 
	$ADMIN->add( 'localplugins', $settings );
 
	// Add a setting field to the settings for this page
	$settings->add( new admin_setting_configcheckbox(
 
		// This is the reference you will use to your configuration
		'local_trainingtroops/bac_on',
 
		// This is the friendly title for the config, which will be displayed
		'Activer',
 
		// This is helper text for this config field
		'Activer la création de bacs à sable sur cette plate-forme',
 
		// This is the default value
		0,
 
		// This is the type of Parameter this config is
		PARAM_BOOL
 
	) );
 
}*/

