<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox ajax controller
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_trainingtroops\local\controllers;
use local_trainingtroops\local\models\sandbox_mail;
use local_trainingtroops\local\models\sandbox_entity;
use local_trainingtroops\local\models\troops_array;

use local_mooring\local\controllers\app_controller;
use local_mooring\local\models\app_cohort;
use local_mooring\local\models\app_user;
use local_mooring\local\config;

class sandbox_ajax extends app_controller {

    public function __construct() {
        parent::__construct();
        require_capability('local/trainingtroops:sandbox', $this->context);
        $this->load_model('sandbox_table', 'sandbox');
    }
    
    //Vérifie qu'un établissement existe par son nom
    public function exist() {
        $name = filter_input(INPUT_GET, 'name', FILTER_SANITIZE_STRING);
        return $this->sandbox->exist($name);
    }
    
    //Vérifie qu'un établissement existe par son UAI
    public function exist_by_uai() {
        $uai = filter_input(INPUT_GET, 'uai', FILTER_SANITIZE_STRING);
        return $this->sandbox->exist_by_uai($uai);
    }
    
    //Importe les élèves du fichier SIECLE correspondant
    public function siecle() {
        global $DB, $CFG;
        //On a besoin de ça pour placer les élèves dans une cohorte donnée par son id 
        require_once($CFG->dirroot . '/cohort/lib.php');
        
        $uai = filter_input(INPUT_POST, 'uai', FILTER_SANITIZE_STRING);
        
        //On récupère l'id des deux classes
        $classes_id = $DB->get_records_sql('SELECT c.id FROM {cohort} c '
                . 'INNER JOIN {context} ct ON c.contextid = ct.id '
                . 'INNER JOIN {course_categories} cc ON ct.instanceid = cc.id '
                . 'WHERE cc.name = :uai ORDER BY c.id',
                ['uai' => $uai]);      
        if(count($classes_id) != 2) return (object) ['error' => 'Les classes de ce bac à sable sont invalides.'];
        $classe1_id = array_keys($classes_id)[0];
        $classe2_id = array_keys($classes_id)[1];   
        
        $new_students = [];
        $new_students_html = '';
        $existing_students = [];
        $existing_students_html = '';
        
        $troops = (new troops_array())->getTroops($uai);
        foreach($troops as $i => $troop){
            $user = (object) [
                'profil'    => 'student',
                'username'  => null,
                'lastname'  => $troop['nom'],
                'firstname' => $troop['prenom'],
                'email'     => 'support.elea@ac-versailles.fr-invalid',
                'hash'      => password_hash('eleve',PASSWORD_BCRYPT),
                'siecle'    => $troop['id'],
                'special'   => null,
                'lilie'     => null
            ];

            $updtpasswd = true;
            $cohorts = [$troop['classe']];

            $this->load_model('school_table', 'school');
            $user->uai = $uai;
            $user->timestamp = time();
            $cas = '';

            $userobj = new app_user($user, $cas);
            $status = $userobj->update_or_create($updtpasswd);
            $userid = $userobj->get_userid();
            
            cohort_add_member($i%2 ? $classe1_id : $classe2_id, $userid);
            
            switch($status){
                case 'created':
                    $new_students[] = $userobj->get_username();
                    $new_students_html .= '<li>'.$userobj->get_username().'</li>';
                break;
                default:
                    $existing_students[] = $userobj->get_username();
                    $existing_students_html .= '<li>'.$userobj->get_username().'</li>';
                break;
            }
        }
        
        return (object) [
            'success'           => true, 
            'new'               => $new_students, 
            'newhtml'          => $new_students_html,
            'newcount'         => count($new_students),
            'existing'          => $existing_students,
            'existinghtml'     => $existing_students_html,
            'existingcount'    => count($existing_students),
        ];
    }
    
    //Permet à un formateur de s'enroler dans tous les parcours d'un bac à sable
    public function enrol(){
        global $USER, $DB, $CFG;
        
        $data = (object) filter_input_array(INPUT_POST, [
            'uai'  => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
        ]);
        
        //Récupérer l'établissement dont on donne le nom
        $sandbox = $this->sandbox->one_by_uai($data->uai);
        if($sandbox->who != $USER->username && !is_siteadmin($USER->id)) return (object) ['error' => 'Vous n\'avez pas l\'autorisation de vous inscrire aux parcours de ce bac à sable.'];
        
        //Récupérer la liste des cours de l'établissement
        $this->load_model('sandbox_table', 'sandbox');
        $parcours = $this->sandbox->one_by_uai($data->uai)->info->parcours;
        foreach($parcours as $p){
            require_once $CFG->dirroot . '/enrol/manual/lib.php';
            $instance = new \enrol_manual_plugin();
            $roleid = $DB->get_record('role', ['shortname' => 'editingteacher'], 'id', MUST_EXIST)->id;
            $manual = $DB->get_record('enrol', ['courseid' => $p->id, 'enrol' => 'manual']);
            if (!$manual){
                $manual = $instance->add_instance((object) ['id' => $enrol->courseid]);
            }
            $instance->enrol_user($manual, $USER->id, $roleid);
        }
        return (object) ['success' => true];
    }
    
    //Inscrit les enseignants d'un bac à sable dans les parcours demandés
    public function enrolFormateur(){
        global $USER, $DB, $CFG;
        
        $data = (object) filter_input_array(INPUT_POST, [
            'uai'  => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
        ]);
        
        //Vérifier que l'UAI correspond bien à un bac à sable de l'utilisateur
        $sandbox = $this->sandbox->one_by_uai($data->uai);
        if($sandbox->who != $USER->username && !is_siteadmin($USER->id)) return (object) ['error' => 'Vous n\'avez pas l\'autorisation d\'inscrire des enseignants dans ce bac à sable.'];
     
        //Vérifier que les parcours appartiennent bien au dossier "Formation" de l'utilisateur
        if(!isset($_POST['parcours'])) return (object) ['error' => 'Vous n\'avez pas sélectionné de parcours.'];
        $parcours = (array) $_POST['parcours'];
        foreach($parcours as $p){
            if(!array_key_exists($p,$sandbox->get_parcours_formateur())) return (object) ['error' => 'La liste des parcours est invalide.'];
        }
        
        //Récupérer la liste des enseignants
        $teachers = $sandbox->get_info()->profs;
       
        //Pour chaque parcours, inscrire chaque enseignant
        foreach($parcours as $p){
            foreach($teachers as $t){
                require_once $CFG->dirroot . '/enrol/manual/lib.php';
                $instance = new \enrol_manual_plugin();
                $roleid = $DB->get_record('role', ['shortname' => 'teacher'], 'id', MUST_EXIST)->id;
                $manual = $DB->get_record('enrol', ['courseid' => $p, 'enrol' => 'manual']);
                if (!$manual){
                    $manual = $instance->add_instance((object) ['id' => $enrol->courseid]);
                }
                $instance->enrol_user($manual, $t['id'], $roleid);
            }
        }
        
        return (object) ['success' => true];
    }
    
    //Envoie un mail à tous les professeurs d'un bac à sable
    public function mail(){
        global $USER, $DB;
        
        $data = (object) filter_input_array(INPUT_POST, [
            'uai'  => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
            'message'  => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
        ]);
        
        //Récupérer l'établissement dont on donne l'UAI
        $sandbox = $this->sandbox->one_by_uai($data->uai);
        if($sandbox->who != $USER->username && !is_siteadmin($USER->id)) return (object) ['error' => 'Vous n\'avez pas l\'autorisation de supprimer cet établissement.'];
        
        //Récupérer la liste des professeurs de l'établissement
        $mail = new sandbox_mail();
        $profs = $DB->get_records_sql('SELECT u.id,u.username from {user} u INNER JOIN {user_info_data} d ON d.userid = u.id '
                . 'WHERE u.username LIKE :uai AND d.fieldid = :profil AND d.data = "teacher"',[
                    "profil" => config::load()->get_user_field_id('profil'),
                    "uai" => substr($sandbox->uai,3,4).'%'
                ]);

        //Envoyer un mail à tout le monde
        foreach($profs as $prof){
            $mail->send($prof->id, $USER->id, 'Formation Éléa', $data->message);
        }
        
        return (object) ['success' => true];
    }
    
    //Supprime un bac à sable
    public function delete(){
        global $USER, $CFG, $DB;
        require_once($CFG->dirroot . '/user/lib.php');
        //On récupère le nom du bac à sable voulu et les infos
        $data = (object) filter_input_array(INPUT_POST, [
            'uai'                  => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
        ]);
        
        //Vérifier que le compte qui supprime est soit admin, soit le compte qui l'a créé
        $sandbox = $this->sandbox->one_by_uai($data->uai);
        if($sandbox->who != $USER->username && !is_siteadmin($USER->id)) return (object) ['error' => 'Vous n\'avez pas l\'autorisation de supprimer cet établissement.'];
        
        //Supprimer le manager
        $manager = current($DB->get_records_list('user', 'username', [$sandbox->uai]));
        user_delete_user($manager);
        
        //Suspendre les comptes élève
        $DB->execute('UPDATE {user} u
                  INNER JOIN {user_info_data} da
                          ON da.userid = u.id
                         AND da.fieldid = :fieldprofil
                         AND da.data = "student"
                  INNER JOIN {user_info_data} db
                          ON db.userid = u.id
                         AND db.fieldid = :fielduai
                         AND db.data = :uai
                         SET suspended = 1',[
            "fieldprofil" => config::load()->get_user_field_id('profil'),
            "fielduai" => config::load()->get_user_field_id('uai'),
            "uai" => $data->uai,
            ]);


        //Renvoyer un message
        return (object) ['success' => true];
    }
    
    //Crée ou met un bac à sable à jour (pour y ajouter des enseignants)
    public function create() {     
        global $CFG, $DB, $USER;
        
        //On arrête tout si la création de bacs à sable n'est pas autorisée ici
        //if(!get_config('local_trainingtroops', 'bac_on')) return (object) ['error' => 'La création de bacs à sable est désactivée.'];
        
        //On a besoin de ça pour placer les élèves dans une cohorte donnée par son id 
        require_once($CFG->dirroot . '/cohort/lib.php');
        
        $this->load_model('manager_user', 'manager');
        $this->load_model('teacher_user', 'teacher');
        
        //On récupère le nom du bac à sable voulu et les infos
        $data = filter_input_array(INPUT_POST, [
            'uai'                  => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
            'name'                  => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
            'mails'                 => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
            'nombre_enseignants'    => (int) array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
            'mode'                  => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
        ]);
        
        //On détermine l'UAI qu'aura l'établissement créé
        if(!isset($data['uai'])) $data['uai'] = $this->sandbox->generateUai();
        $data = (object) $data;
        $prefixe = substr($data->uai,3,4);
        
        //En mode édition, l'établissement, le référent et les deux classes sont déjà créés.
        //Il faut vérifier que l'établissement existe déjà, sinon on s'arrête.
        //Il faut également retrouver les ids des deux classes.
        if($data->mode == 'edition'){
            //On récupère le bac à sable que l'on veut modifier, puis l'UAI et le préfixe
            $sandbox = $this->sandbox->one_by_uai($data->uai);
            if(!$sandbox) return (object) ['error' => 'L\'établissement n\'existe pas.'];
            if($sandbox->who != $USER->username && !is_siteadmin($USER->id)) return (object) ['error' => 'Vous n\'avez pas l\'autorisation de modifier cet établissement.'];
            $data->uai = $sandbox->uai;
            $prefixe = substr($sandbox->uai,3,4);
            
            //On récupère l'id des deux classes
            $classes_id = $DB->get_records_sql('SELECT c.id FROM {cohort} c '
                    . 'INNER JOIN {context} ct ON c.contextid = ct.id '
                    . 'INNER JOIN {course_categories} cc ON ct.instanceid = cc.id '
                    . 'WHERE cc.name = :uai ORDER BY c.id',
                    ['uai' => $sandbox->uai]);      
            if(count($classes_id) != 2) return (object) ['error' => 'Les classes de ce bac à sable sont invalides.'];
            $classe1_id = array_keys($classes_id)[0];
            $classe2_id = array_keys($classes_id)[1];          
        }
        
        //On récupère les profs à ajouter depuis les mails fournis. Si ça se passe mal, on renvoie une erreur.
        $profs = $this->extractProfs($data->mails);
        if($profs === false) return (object) ['error' => 'Les adresses académiques indiquées pour les enseignants sont invalides.'];
        if($data->nombre_enseignants > 50) return (object) ['error' => 'Le nombre d\'enseignants à créer ne doit pas dépasser 50.'];
        
        //On vérifie que les profs à ajouter par mail ne sont pas déjà présents
        foreach($profs as $prof){
            $username = $this->teacher->generateUsername($prof,$data->uai);
            if($DB->get_record('user', ['username' => $username]) || 
                  $DB->get_record('user', ['username' => $username.'.eleve'])){
                return (object) ['error' => 'Une des adresses acaémiques correspond à un professeur qui existe déjà.'];
            }
        }
        
        //On ajoute les enseignants automatiques en s'assurant que leur username n'existe pas déjà
        $rang = 0;
        for($i = 0; $i < $data->nombre_enseignants; $i++){ 
            do $rang++; 
            while($DB->get_record('user', ['username' => $prefixe.'.prof'.$rang]) || 
                  $DB->get_record('user', ['username' => $prefixe.'.prof'.$rang.'.eleve'])); 
            
            $profs[] = [
                'mail' => 'support-elea@ac-versailles.fr.invalid', 
                'prenom' => 'Prof', 
                'nom' => $rang
            ];      
        }
        
        //En mode addition, on crée l'établissement et ses référents. 
        //Il faut pour cela que l'établissement n'existe pas déjà dans la plate-forme.
        if($data->mode == 'addition'){
            //On crée l'objet permettant de créer les deux classes une fois qu'on a l'UAI
            $this->cohort = new app_cohort($data->uai);
            
            //On vérifie que ce nom d'établissement n'existe pas déjà
            if($this->sandbox->exist_by_uai($data->uai)) return (object) ['error' => 'Un établissement portant le même nom existe déjà sur la plate-forme.'];
            
            //Création de l'établissement et de son référent
            $sandbox = $this->sandbox->create($data);
            $this->manager->sandbox_create($data);
            
            $prefixe = substr($sandbox->uai,3,4);
            
            //Création des deux classes
            $classe1_id = $this->cohort->create('Classe 1');
            $classe2_id = $this->cohort->create('Classe 2');
        }
        
        /*
         * On crée les professeurs demandés.
         */
        $i = 1;
        foreach($profs as $i => $prof){
            //Création du prof dans le bon établissement
            $newTeacher = $this->teacher->teacher_create($sandbox,$prof);
            $profs[$i]['username'] = $newTeacher->username;
            $profs[$i]['clearPassword'] = $newTeacher->clearPassword;
            //Création de l'élève correspondant au prof -- DÉSACTIVÉ (elea/platform#361)
            /*$eleve = (object) [
                'firstname' => $newTeacher->firstname,
                'lastname' => $newTeacher->lastname,
                'profil' => 'student',
                'uai' => $sandbox->uai,
                'username' => $newTeacher->username.'.eleve',
                'hash' => password_hash('eleve',PASSWORD_BCRYPT),
                'email' => $newTeacher->email,             
            ];
            $newStudent = new app_user($eleve,false);
            $newStudent->create();*/
            //Placement de l'élève dans la classe 1 ou 2
            //cohort_add_member($i%2 ? $classe1_id : $classe2_id, $newStudent->get_userid());
            //
            //Placement du prof dans la classe 1 ou 2
            //cohort_add_member($i%2 ? $classe1_id : $classe2_id, $newTeacher->id);
            
            $i++;
        }

        //On renvoie des informations sur ce qui a été créé, pour l'affichage.
        $ret = (object) [
            'success' => true,
            'uai' => $sandbox->uai,
            'mdp_referent' => 'referent',
            'name' => $sandbox->name,
            'nb_profs' => count($profs),
            'prefixe_prof' => $prefixe,
            'mdp_prof' => 'prof',
            'premier_prof' => count($profs) ? ($prefixe.'.'.$profs[0]['prenom'].'.'.$profs[0]['nom']) : '',
            'mdp_eleve' => 'eleve',
            'nb_profs_generes' => $data->nombre_enseignants,
            'profs' => $profs
        ];
        
        return $ret;              
    }
    
    //Extrait les adresses mails de profs fournies dans un textarea. Récupère le prénom et le nom de chaque prof.
    public function extractProfs($mails){
        
        //On supprime les espaces blancs, puis on transforme les délimiteurs pour que ce soit des points-virgules
        $mails = str_replace(" ", '', $mails);
        $mails = str_replace(array("\r\n","\n\r","\r","\n",","), ';', $mails);
        $mails = str_replace(" ", '', $mails);
        $mails = explode(';',strtolower($mails));
        
        //On vérifie qu'il n'y a pas de doublons (le nom d'utilisateur doit être unique)
        if(array_unique($mails) !== $mails) return false;
        
        //On construit le tableau des profs suivant les mails
        $profs = [];
        foreach($mails as $mail){
            if($mail){
                //Si un mail est invalide
                if(!filter_var($mail, FILTER_VALIDATE_EMAIL)) return false;

                //Si un mail n'est pas en ac-versailles.fr
                $mailParts = explode('@', $mail);
                if($mailParts[1] != 'ac-versailles.fr') return false;
                
                //Si tout est bon, on récupère le prénom, le nom et l'email
                $fullName = explode('.',$mailParts[0]);
                $prof = [
                    'mail' => $mail, 
                    'prenom' => $fullName[0], 
                    'nom' => $fullName[1]
                ];
                
                $profs[] = $prof;
            }   
        }
        
        return $profs;
    }
    
}
