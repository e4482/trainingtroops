<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox modal controller
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_trainingtroops\local\controllers;

use local_mooring\local\controllers\app_controller;

class sandbox_modal extends app_controller {
    
    public function __construct() {
        parent::__construct();
        require_capability('local/trainingtroops:sandbox', $this->context);
    }
    
    public function addition() {
        $this->render('sandbox.addition');
    }
    
    public function edition() {
        $this->load_model('sandbox_table', 'sandbox');
        $this->uai = filter_input(INPUT_GET, 'uai', FILTER_SANITIZE_STRING);
        $this->render('sandbox.edition',['sandbox' => $this->sandbox->one_by_uai($this->uai)->info]);
    }
    
}
