<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox table model
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_trainingtroops\local\models;

use local_mooring\local\models\core_table as core_table;
use local_mooring\local\library;

class sandbox_table extends core_table {
    
    protected $table = 'local_trainingtroops_sandbox';
    
    public function all() {
        global $DB, $USER;
        //Récupérer les bacs à sable créés par l'utilisateur qui ont encore un référent (ils n'ont donc pas encore été supprimés)
        $sandbox = $DB->get_records_sql('SELECT s.* from {'.$this->table.'} s INNER JOIN {user} u ON u.username = s.uai WHERE s.who = ? AND s.uai LIKE "000%" AND u.deleted = 0 ORDER BY time DESC',[$USER->username]);
        $sandbox = $this->to_entities($sandbox);
        return $sandbox;
    }
    
    public function one($name) {
        global $DB;
        $sandbox = $DB->get_record($this->table, ['name' => $name]);
        if ($sandbox) {
            return $this->to_entity($sandbox);
        }
        return false;
    }
    
    public function exist($name) {
        $sandbox = $this->one($name);
        if (isset($sandbox->name)) {
            return true;
        }
        return false;
    }
          
    public function one_by_uai($uai) {
        global $DB;
        $sandbox = $DB->get_record($this->table, ['uai' => $uai]);
        if ($sandbox) {
            return $this->to_entity($sandbox);
        }
        return false;
    }
        
    public function exist_by_uai($uai) {
        $sandbox = $this->one_by_uai($uai);
        if (isset($sandbox->uai)) {
            return true;
        }
        return false;
    }
    
    /*public function cas($uai, $nature) {
        return config::load('cas')->is_any_cas($uai, $nature);
    }*/
    
    public function create($data) {
        global $DB, $USER;
        
        library::test_uai($data->uai);
        $data->uai = strtolower($data->uai);
        
        if ($DB->get_record($this->table, array ('uai' => $data->uai), 'id')) {
            throw new \Exception("SANDBOX: already exists");
        }
        
        //$data->cas = library::is_any_cas($data->uai, $data->nature);
        $data->time = time();
        $data->who = $USER->username;
        
        $DB->insert_record($this->table, $data);
        
        return $data;
    }
    
    public function generateUai(){
        global $DB;
        $letter = 'f';
        $lastSandbox = $DB->get_records_sql('SELECT id,uai FROM {'.$this->table.'} WHERE uai LIKE ? ORDER BY id DESC LIMIT 1',['000%']);
        if($lastSandbox){
            $lastSandbox = current($lastSandbox);
            $numbers = (int) substr($lastSandbox->uai,3,4);
            $numbers = ($numbers + 1) % 10000;
            $numbers = str_pad($numbers, 4, '0', STR_PAD_LEFT);
        }
        else $numbers = '0001';
        $CHARS = 'abcdefghjklmnprstuvwxyz';
        $modulo = ('000'.$numbers) % strlen($CHARS);
        $letter = $CHARS[$modulo];
        return '000'.$numbers.$letter;
    }
    
}
