<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox manager model
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_trainingtroops\local\models;

use local_mooring\local\models\core_user;

class manager_user extends core_user {
    
    public function sandbox_create($sandbox) {
        global $DB;


        $user = new \stdClass();
        $user->username = $sandbox->uai;
        $user->firstname = 'Gestion';
        $user->lastname = $sandbox->name;
        $user->email = 'ce.' . $sandbox->uai . '@ac-versailles.fr';
        $user->password = password_hash('referent',PASSWORD_BCRYPT);

        $user->id = $this->create($user);
        
        $extra = (object) [
            'profil'    => 'schoolmanager',
            'uai'       => $sandbox->uai
        ];
        $this->set_fields($user->id, $extra);

        $role = $DB->get_record('role', ['shortname' => 'schoolmanager'], 'id');
        role_assign($role->id, $user->id, \context_system::instance());

        return $user;
    }
    
}

