<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox entity model
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_trainingtroops\local\models;

use local_mooring\local\models\core_entity;
use local_mooring\local\config;

class sandbox_entity extends core_entity {
    
    private $users;
    private $uprofs = [];
    private $parcours = [];
    private $parcours_formateur = [];
    private $parcours_formation_page_name = "Formation";
    
    public function __construct($array) {
        parent::__construct($array);
        $this->search_users();
        $this->prepare_profs();
        $this->search_parcours();
        $this->search_parcours_formateur();
    }
    
    private function search_users() {
        global $DB;
        $users = $DB->get_records_sql('SELECT u.username, u.id
                                         FROM {user} u
                                   INNER JOIN {user_info_data} d
                                           ON d.userid = u.id
                                        WHERE d.data = :uai
                                          AND d.fieldid = :field
                                     ORDER BY u.id'
                ,[
                    "field" => config::load()->get_user_field_id('uai'),
                    "uai" => $this->uai
        ]);
        
        $this->users = $users;
    }
    
    private function prepare_profs() {
        foreach ($this->users as $user) {
            if (substr($user->username,0,5) === substr($this->uai, 3, 4) . '.' && strpos($user->username, '.eleve') === false) {
                $this->uprofs[] = [
                    'id'                => $user->id,
                    'username'          => $user->username,
                    //'classe'            => $this->users[$user->username]->cohort
                    //'username_eleve'  => $user->username . '.eleve',
                    //'classe_eleve'    => $this->users[$user->username . '.eleve']->cohort
                ];
            }
        }
    }
    
    private function search_parcours(){
        global $DB;
        $sql = 'SELECT c.id, c.fullname
                  FROM {course} c
             LEFT JOIN {course_categories} cc
                    ON c.category = cc.id
                 WHERE cc.name = :uai';
        $this->parcours = $DB->get_records_sql($sql, [
            'uai' => $this->uai
        ]);
    }
    
    private function search_parcours_formateur(){
        global $DB, $USER;
        $sql = 'SELECT c.id, c.fullname
                  FROM {course} c
             LEFT JOIN {teacherboard_page_items} tpi
                    ON c.id = tpi.courseid
             LEFT JOIN {teacherboard_page} tp
                    ON tpi.pageid = tp.id
                 WHERE tp.userid = :userid
                   AND tp.name = :pagename'
                ;
        $this->parcours_formateur = $DB->get_records_sql($sql, [
            'userid' => $USER->id,
            'pagename'=> $this->parcours_formation_page_name
        ]);
        
    }
    
    public function get_info(){
        $info_array= [
            'uai' => $this->uai,
            'name' => $this->name,
            'profs' => $this->uprofs,
            'nb_profs' => $this->profs,
            'nb_eleves' => $this->eleves,
            'parcours' => $this->parcours,
            'parcours_formateur' => $this->parcours_formateur,
            //'nb_eleves_importes' => 0
        ];
        
        return (object) $info_array;
    }
    
    public function get_profs() {
        return count($this->uprofs);
    }
    
    public function get_eleves() {
        return count($this->users) - count($this->uprofs) - 1;
    }
    
    public function get_parcours_formateur(){
        return $this->parcours_formateur;
    }
    
}
