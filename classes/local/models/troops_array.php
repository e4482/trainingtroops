<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Teacher model
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_trainingtroops\local\models;

class troops_array{
    
    public function getTroops($uai){
        $troops = [
            [
                'nom' => 'Rico',
                'prenom' => 'Johnny',
            ],
            [
                'nom' => 'Rico',
                'prenom' => 'Emilio',
            ],
            [
                'nom' => 'Ibanez',
                'prenom' => 'Carmen',
            ],
            [
                'nom' => 'Flores',
                'prenom' => 'Dizzy',
            ],
            [
                'nom' => 'Jenkins',
                'prenom' => 'Carl',
            ],
            [
                'nom' => 'Jenkins',
                'prenom' => 'Al',
            ],
            [
                'nom' => 'Zim',
                'prenom' => 'Charles',
            ],
            [
                'nom' => 'Deladrier',
                'prenom' => 'Yvette',
            ],
            [
                'nom' => 'Dubois',
                'prenom' => 'Jean',
            ],
            [
                'nom' => 'Bamburger',
                'prenom' => 'Wilhem',
            ],
            [
                'nom' => 'Rasczak',
                'prenom' => 'Jean',
            ],
            [
                'nom' => 'Frankel',
                'prenom' => 'Ian',
            ],
            [
                'nom' => 'Alphard',
                'prenom' => 'Smith',
            ],
            [
                'nom' => 'Leivy',
                'prenom' => 'Pat',
            ],
            [
                'nom' => 'Hendrick',
                'prenom' => 'Theodore',
            ],
        ];
        foreach($troops as $i => $troop){
            $troops[$i]['id'] = ($i+1).strtoupper($uai);
            $troops[$i]['classe'] = 'Classe '.((($i+1)%2)+1);
        }
        return $troops;
    }
    
}