<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Teacher model
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_trainingtroops\local\models;

use local_mooring\local\models\core_user;

class teacher_user extends core_user {
    
    public $user;
    public function __construct(){
        $this->user = new \stdClass();
    }
    
    public function teacher_create($sandbox,$info) {
        global $DB;

        $this->user->username = $this->generateUsername($info,$sandbox->uai);
        $this->user->firstname = $info['prenom'];
        $this->user->lastname = $info['nom'];
        $this->user->email = $info['mail'];
        $this->user->clearPassword = $this->generateRandomString(8);
        $this->user->password = password_hash($this->user->clearPassword,PASSWORD_BCRYPT);
        
        $this->user->id = $this->create($this->user);
        
        $extra = (object) [
            'profil'    => 'teacher',
            'uai'       => $sandbox->uai,
            'rne1'       => $sandbox->uai,
        ];
        $this->set_fields($this->user->id, $extra);

        return $this->user;
    }

    function generateRandomString($length = 8) {
        $characters = '23456789abcdefghkmnpqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    public function generateUsername($info,$uai){
        $prefixe = substr($uai, 3, 4);
        if(is_numeric($info['nom'])){ 
            return $prefixe.'.'.strtolower($info['prenom']).$info['nom'];
        }
        return $prefixe.'.'.strtolower($info['prenom']).($info['nom'] ? '.'.$info['nom'] : '');
    }
    
}

