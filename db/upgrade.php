<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Capabilities
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_trainingtroops_upgrade($oldversion) {
    global $DB;
    $dbman = $DB->get_manager();
    
    if ($oldversion < 2017110900) {
        // Define table local_trainingtroops_sandbox to be created.
        $table = new xmldb_table('local_trainingtroops_sandbox');

        // Adding fields to table local_trainingtroops_sandbox.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('uai', XMLDB_TYPE_CHAR, '8', null, XMLDB_NOTNULL, null, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('time', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('who', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table local_trainingtroops_sandbox.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table local_trainingtroops_sandbox.
        $table->add_index('uai', XMLDB_INDEX_UNIQUE, array('uai'));
        $table->add_index('who', XMLDB_INDEX_NOTUNIQUE, array('who'));

        // Conditionally launch create table for local_trainingtroops_sandbox.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Trainingtroops savepoint reached.
        upgrade_plugin_savepoint(true, 2017110900, 'local', 'trainingtroops');
    }
    
    return true;
}
