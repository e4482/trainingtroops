// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox index script
 *
 * @package     trainingtroops
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require(['local_mooring/URI', 'local_mooring/ajax'], function(URI, ajax){  
    var viewModifier = {
        
        $modalLoadEdition: function(html) {
            
            //Apparition du modal pour modifier un bac à sable
            var $modal = $('#modal-edition')
            $modal.html(html)
            $modal.modal('show')
            
            //Afficher/masquer le tableau des enseignants
            $modal.find('#afficher-table-enseignants').click(function(e){
                e.preventDefault()
                if($('#table-enseignants').is(':visible')){
                    $('#table-enseignants').hide();
                    $('#afficher-table-enseignants').html('Afficher les enseignants... <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>');
                }
                else{
                    $('#table-enseignants').show();
                    $('#afficher-table-enseignants').html('Masquer les enseignants... <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>');
                }
            })
            
            //Afficher/masquer le tableau des parcours
            $modal.find('#afficher-table-parcours').click(function(e){
                e.preventDefault()
                if($('#table-parcours').is(':visible')){
                    $('#table-parcours').hide();
                    $('#afficher-table-parcours').html('Afficher les parcours... <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>');
                }
                else{
                    $('#table-parcours').show();
                    $('#afficher-table-parcours').html('Masquer les parcours... <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>');
                }
            })
            
            //Liaison de l'événement pour ajouter les enseignants
            $modal.find('#ajouter-enseignants').click(function(e) {
                e.preventDefault()
                var mails = $modal.find('#form-generer-mails').val().trim()
                var nombre_enseignants = parseInt($modal.find('#form-nombre-enseignants').val())
                if(mails || nombre_enseignants)
                {

                    $modal.find('button').prop('disabled', true)
                    controller.edition({
                        uai: $modal.find('#form-sandbox-uai').val().trim(),
                        mails: mails,
                        nombre_enseignants: nombre_enseignants,
                        mode: 'edition',
                    })
                    viewModifier.$modalAlert('edition', 'ajout-enseignants-form', 'info', "Ajout des enseignants...") 
                }
            })
            
            //Liaison de l'événement pour le bouton de suppression
            $modal.find('#supprimer-bac-ok').click(function(e) {
                e.preventDefault()
                $modal.find('button').prop('disabled', true)
                controller.suppression({
                    uai: $modal.find('#form-sandbox-uai').val().trim(),
                })
                viewModifier.$modalAlert('edition', 'supprimer-bac', 'info', "Suppression du bac à sable...") 
            })
            
            //Liaison de l'événement pour l'import d'élèves
            $modal.find('#import-siecle-ok').click(function(e) {
                e.preventDefault()
                //$modal.find('button').prop('disabled', true)
                controller.importSiecle({
                    uai: $modal.find('#form-sandbox-uai').val().trim(),
                })
                viewModifier.$modalAlert('edition', 'import-siecle', 'info', "Importation des élèves en cours <strong>(cela peut prendre du temps)</strong>...") 
            })
            
            //Liaison de l'événement pour le bouton d'enrôlement
            $modal.find('#injecter-cours').click(function(e) {
                e.preventDefault()
                $modal.find('button').prop('disabled', true)
                controller.enrol({
                    uai: $modal.find('#form-sandbox-uai').val().trim(),
                })
                viewModifier.$modalAlert('edition', 'parcours', 'info', "Injection dans les parcours...") 
            })
            
            //Liaison de l'événement pour le bouton d'enrôlement
            $modal.find('#injecter-cours-formateur').click(function(e) {
                e.preventDefault()
                //$modal.find('button').prop('disabled', true)
                parcours = []
                $.each($modal.find('.parcours-formateur'),function(index, elt){
                    if(elt.checked) parcours.push(elt.value)
                })
                controller.enrolFormateur({
                    uai: $modal.find('#form-sandbox-uai').val().trim(),
                    parcours: parcours
                })
                viewModifier.$modalAlert('edition', 'parcours', 'info', "Injection des enseignants dans les parcours sélectionnés...") 
            })
            
            //Liaison de l'événement pour le bouton de mail
            $modal.find('#mail-envoyer').click(function(e) {
                e.preventDefault()
                var message = $modal.find('#form-message').val().trim()
                if(message)
                {
                    $modal.find('button').prop('disabled', true)
                    controller.mail({
                        uai: $modal.find('#form-sandbox-uai').val().trim(),
                        message: message,
                    })
                    viewModifier.$modalAlert('edition', 'mail-form', 'info', "Envoi des mails...") 
                }
            })
            
            $modal.on("hidden.bs.modal", function () {
                window.location.reload()
            })

            $('#ajout-enseignants-print').click(function(e){
                e.preventDefault()
                viewModifier.$print()
            })
            
            
        },
        
        //Changement d'étape dans le modal
        $modalChangeStep: function(modal, step) {
            var selector = '#modal-' + modal
            var $steps = $(selector).find('div[data-step]')
            $steps.each(function(index) {
                if ($(this).data('step') === step) {
                    $(this).show()
                }
                else
                    $(this).hide()
            })
        },
        
        //Affichage d'une alerte dans le modal
        $modalAlert: function(modal, step, type, msg) {
            var selector = '#modal-' + modal + ' div[data-step="' + step + '"]'
            var $alert = $(selector).find('.alert:first')
            $alert.hide().removeClass('alert-success alert-warning alert-danger')
            $alert.addClass('alert-' + type).html(msg).show()
            $(selector).find('button[type="submit"]').prop('disabled', false)
        },
        
        $setStepData: function(data){
            //Affichage des données, affichage/masquage de certains éléments
            for(var key in data){
                var val = data[key]
                $('.form-data-'+key).html(val)
                if(val && parseInt(val) !== 0){
                    $('.form-data-show-'+key).show()
                    $('.form-data-hide-'+key).hide()
                }
                else{
                    $('.form-data-show-'+key).hide()
                    $('.form-data-hide-'+key).show()
                }
            }
        },

        $teacherLoginTable: function(teachers){
            if(teachers.length) {
                var table = '<table style="width:100%;">'
                table += '<tr><th>Identifiant</th><th>Mot de passe</th></tr>'
                for (var t of teachers) {
                    table += '<tr><td>' + t.username + '</td><th>' + t.clearPassword + '</th></tr>'
                }
                table += '</table>'
                return table
            }
            return  ''
        },
        $print: function(){
            var w = window.open()
            if(w) {
                w.document.write(this.lastLoginTable)
                w.print()
            }
        },
        lastLoginTable: ''
        
    }
    var ajaxRequest = {
        
        getModal: function(modal,uai) {
            return ajax.get('?way=sandbox.modal.' + modal,{uai: uai})
        },
        
        sandboxEdit: function(data) {
            return ajax.post('?way=sandbox.ajax.create', data).then(ajax.handle)
        },
        
        courseEnrol: function(data) {
            return ajax.post('?way=sandbox.ajax.enrol', data).then(ajax.handle)
        },
        
        courseEnrolFormateur: function(data) {
            return ajax.post('?way=sandbox.ajax.enrolformateur', data).then(ajax.handle)
        },
        
        sandboxDelete: function(data) {
            return ajax.post('?way=sandbox.ajax.delete', data).then(ajax.handle)
        },
        
        sandboxMail: function(data) {
            return ajax.post('?way=sandbox.ajax.mail', data).then(ajax.handle)
        },
        
        importSiecle: function(data) {
            return ajax.post('?way=sandbox.ajax.siecle', data).then(ajax.handle)
        }
        
    }
    
    
    var controller = {
        
        loading: function(uai,step) {
            ajaxRequest.getModal('edition',uai).then(function(html) {
                viewModifier.$modalLoadEdition(html)
                viewModifier.$modalChangeStep('edition', step)
            }).catch(function(error) {
                console.log(error)
            })
        },
        
        suppression: function(data){
            ajaxRequest.sandboxDelete(data).then(function(data){
                viewModifier.$modalChangeStep('edition', 'supprimer-resultat')
                if(data){
                    if(data.error){
                        viewModifier.$setStepData(data)
                        viewModifier.$modalAlert('edition', 'supprimer-resultat', 'danger', "Une erreur est survenue lors de la suppression du bac à sable...")
                    }
                    else{
                        viewModifier.$modalAlert('edition', 'supprimer-resultat', 'success', "Le bac à sable a bien été supprimé.")         
                    }
                }
                else{
                    viewModifier.$modalAlert('edition', 'supprimer-resultat', 'danger', "Une erreur est survenue lors de la suppression du bac à sable...")
                }
            }).catch(function(data){
                viewModifier.$modalChangeStep('edition', 'supprimer-resultat')
                viewModifier.$modalAlert('edition', 'supprimer-resultat', 'danger', "Une erreur est survenue lors de la suppression du bac à sable...")
            })
        },
        
        mail: function(data){
            ajaxRequest.sandboxMail(data).then(function(data){
                viewModifier.$modalChangeStep('edition', 'mail-resultat')
                if(data){
                    if(data.error){
                        viewModifier.$setStepData(data)
                        viewModifier.$modalAlert('edition', 'mail-resultat', 'danger', "Une erreur est survenue lors de l'envoi des mails...")
                    }
                    else{
                        viewModifier.$modalAlert('edition', 'mail-resultat', 'success', "Les mails ont été bien envoyés.")         
                    }
                }
                else{
                    viewModifier.$modalAlert('edition', 'mail-resultat', 'danger', "Une erreur est survenue lors de l'envoi des mails...")
                }
            }).catch(function(data){
                viewModifier.$modalChangeStep('edition', 'mail-resultat')
                viewModifier.$modalAlert('edition', 'mail-resultat', 'danger', "Une erreur est survenue lors de l'envoi des mails...")
            })
        },
        
        enrol: function(data){
            ajaxRequest.courseEnrol(data).then(function(data){
                viewModifier.$modalChangeStep('edition', 'injection')
                if(data){
                    if(data.error){
                        viewModifier.$modalAlert('edition', 'injection', 'danger', "L'inscription a échoué...")
                    }
                    else{
                        viewModifier.$modalAlert('edition', 'injection', 'success', "Inscription réussie.")         
                    }
                }
                else{
                    viewModifier.$modalAlert('edition', 'injection', 'danger', "L'inscription a échoué...")
                }
            }).catch(function(data){
                viewModifier.$modalChangeStep('edition', 'injection')
                viewModifier.$modalAlert('edition', 'injection', 'danger', "L'inscription a échoué...")
            })
        },
        
        enrolFormateur: function(data){
            ajaxRequest.courseEnrolFormateur(data).then(function(data){
                viewModifier.$modalChangeStep('edition', 'injection-formateur')
                if(data){
                    if(data.error){
                        viewModifier.$setStepData(data)
                        viewModifier.$modalAlert('edition', 'injection-formateur', 'danger', "L'inscription des enseignants a échoué...")
                    }
                    else{
                        viewModifier.$setStepData(data)
                        viewModifier.$modalAlert('edition', 'injection-formateur', 'success', "L'insciption des enseignants a réussi.")         
                    }
                }
                else{
                    viewModifier.$setStepData({error: "Erreur inconnue."})
                    viewModifier.$modalAlert('edition', 'injection-formateur', 'danger', "L'inscription des enseignants a échoué...")
                }
            }).catch(function(data){
                viewModifier.$modalChangeStep('edition', 'injection-formateur')
                viewModifier.$setStepData({error: "Erreur inconnue."})
                viewModifier.$modalAlert('edition', 'injection-formateur', 'danger', "L'inscription des enseignants a échoué...")
            })
        },
        
        //Import d'un fichier SIECLE
        importSiecle: function(data) {
            ajaxRequest.importSiecle(data).then(function(data){
                viewModifier.$modalChangeStep('edition', 'import-siecle-resultat')
                if(data){
                    viewModifier.$setStepData(data)
                    if(data.error){
                        viewModifier.$modalAlert('edition', 'import-siecle-resultat', 'danger', "Une erreur est survenue lors de l'importation des élèves...")
                    }
                    else{
                        viewModifier.$modalAlert('edition', 'import-siecle-resultat', 'success', "L'importation des élèves a réussi !")         
                    }
                }
                else{
                    viewModifier.$modalAlert('edition', 'import-siecle-resultat', 'danger', "Une erreur est survenue lors de l'importation des élèves...")
                }
            }).catch(function(data){
                viewModifier.$modalChangeStep('edition', 'import-siecle-resultat')
                viewModifier.$modalAlert('edition', 'import-siecle-resultat', 'danger', "Une erreur est survenue lors de l'importation des élèves...")
            })
        },
        
        //Lancement de la requête pour la création d'un bac à sable
        edition: function(data) {
            ajaxRequest.sandboxEdit(data).then(function(data){
                viewModifier.$modalChangeStep('edition', 'ajout-enseignants')
                if(data){
                    viewModifier.$setStepData(data)
                    viewModifier.lastLoginTable = viewModifier.$teacherLoginTable(data.profs)
                    $('#ajout-enseignants-table').html(viewModifier.lastLoginTable)
                    //viewModifier.$print()
                    if(data.error){
                        viewModifier.$modalAlert('edition', 'ajout-enseignants', 'danger', "Une erreur est survenue lors de l'ajout des enseignants...")
                    }
                    else{
                        viewModifier.$modalAlert('edition', 'ajout-enseignants', 'success', "Les enseignants ont bien été ajoutés.")         
                    }
                }
                else{
                    viewModifier.$modalAlert('edition', 'ajout-enseignants', 'danger', "Une erreur est survenue lors de l'ajout des enseignants...")
                }
            }).catch(function(data){
                console.log(data)
                viewModifier.$modalChangeStep('edition', 'ajout-enseignants')
                viewModifier.$modalAlert('edition', 'ajout-enseignants', 'danger', "Une erreur est survenue lors de l'ajout des enseignants...")
            })
        },
    }
        
    
    //Apparition du modal
    $('[data-action="edition"]').click(function(e) {
        e.preventDefault()
        controller.loading($(this).attr('data-uai'),$(this).attr('data-step'))
    })
})
