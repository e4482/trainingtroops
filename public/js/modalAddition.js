// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox index script
 *
 * @package     trainingtroops
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require(['local_mooring/URI', 'local_mooring/ajax'], function(URI, ajax){  
    var viewModifier = {
        
        $setStepData: function(data){
            //Affichage des données, affichage/masquage de certains éléments
            for(var key in data){
                var val = data[key]
                $('.form-data-'+key).html(val)
                if(val && parseInt(val) !== 0){
                    $('.form-data-show-'+key).show()
                    $('.form-data-hide-'+key).hide()
                }
                else{
                    $('.form-data-show-'+key).hide()
                    $('.form-data-hide-'+key).show()
                }
            }
        },
        
        $modalLoadAddition: function(html) {
            //Apparition du modal pour ajouter un bac à sable
            var $modal = $('#modal-addition')
            $modal.html(html)
            $modal.modal('show')
            
            //Liaison de l'événement quand les informations ont été envoyées
            $modal.find('#form-generer').submit(function(e) {
                e.preventDefault()
                $modal.find('#form-generer-submit').prop('disabled', true)
                viewModifier.$modalAlert('addition', 'generer', 'info', "Création du bac à sable...")
                controller.create({
                    name: $modal.find('#form-generer-nom').val().trim(),
                    mails: $modal.find('#form-generer-mails').val().trim(),
                    nombre_enseignants: $modal.find('#form-nombre-enseignants').val(),
                    mode: 'addition',
                })
            })
            
            $modal.on("hidden.bs.modal", function () {
                window.location.reload()
            })

            $('#reussite-print').click(function(e){
                e.preventDefault()
                viewModifier.$print()
            })

        },
        
        //Affichage d'une alerte dans le modal
        $modalAlert: function(modal, step, type, msg) {
            var selector = '#modal-' + modal + ' div[data-step="' + step + '"]'
            var $alert = $(selector).find('.alert:first')
            $alert.hide().removeClass('alert-success alert-warning alert-danger')
            $alert.addClass('alert-' + type).html(msg).show()
            $(selector).find('button[type="submit"]').prop('disabled', false)
        },
        
        //Changement d'étage dans le modal
        $modalChangeStep: function(modal, step) {
            var selector = '#modal-' + modal
            var $steps = $(selector).find('div[data-step]')
            var $progress = $(selector).find('.progress-bar:first')
            $steps.each(function(index) {
                if ($(this).data('step') === step) {
                    var percentage = (index + 1) * 100 / $steps.length
                    $progress.css('width', percentage + '%')
                    $progress.text('Étape ' + (index + 1))
                    $(this).show()
                }
                else
                    $(this).hide()
            })
        },

        $teacherLoginTable: function(teachers){
            if(teachers.length) {
                var table = '<table style="width:100%;">'
                table += '<tr><th>Identifiant</th><th>Mot de passe</th></tr>'
                for (var t of teachers) {
                    table += '<tr><td>' + t.username + '</td><th>' + t.clearPassword + '</th></tr>'
                }
                table += '</table>'
                return table
            }
            return  ''
        },
        $print: function(){
            var w = window.open()
            if(w) {
                w.document.write(this.lastLoginTable)
                w.print()
            }
        },
        lastLoginTable: ''

     
    }
    var ajaxRequest = {
        
        getModal: function(modal) {
            return ajax.get('?way=sandbox.modal.' + modal)
        },
        
        //A vérifier sur le serveur plutôt qu'ici
        sandboxNameExist: function(name) {
            return ajax.get('?way=sandbox.ajax.exist', {
                name: name
            }).then(ajax.handle)
        },
        
        sandboxPost: function(data) {
            return ajax.post('?way=sandbox.ajax.create', data).then(ajax.handle)
        }
        
    }
    
    
    var controller = {
        
        loading: function() {
            ajaxRequest.getModal('addition').then(function(html) {
                viewModifier.$modalLoadAddition(html)
            }).catch(function(error) {
                console.log(error)
            })
        },
        
        //Lancement de la requête pour la création d'un bac à sable
        create: function(data) {
            ajaxRequest.sandboxPost(data).then(function(data){
                        if(data.success){
                            viewModifier.$modalChangeStep('addition', 'reussite')
                            viewModifier.$setStepData(data)
                            viewModifier.lastLoginTable = viewModifier.$teacherLoginTable(data.profs)
                            $('#reussite-table').html(viewModifier.lastLoginTable)
                            //viewModifier.$print()
                            $('.form-siecle').attr('href','resources/troops_siecle.php?uai='+data.uai)
                        }
                        else{
                            var error = data.error ? data.error : "Une erreur est survenue lors de la création du bac à sable."
                            viewModifier.$modalAlert('addition', 'generer', 'danger', error)
                        }
                    }).catch(function(data){
                        viewModifier.$modalAlert('addition', 'generer', 'danger', "Une erreur est survenue lors de la création du bac à sable.")
            })
        },
        
    }
    
    //Apparition du modal
    $('[data-action="addition"]').click(function(e) {
        e.preventDefault()
        controller.loading()
    })
})
