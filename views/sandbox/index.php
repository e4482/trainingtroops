<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox index view
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $PAGE, $CFG;
$PAGE->requires->js(new moodle_url('/local/trainingtroops/public/js/modalAddition.js'));
$PAGE->requires->js(new moodle_url('/local/trainingtroops/public/js/modalEdition.js'));
?>

<div class="modal fade" id="modal-addition"></div>
<div class="modal fade" id="modal-edition"></div>

<header class="local">
    <div class="title"><h2>Gestion des bacs à sable</h2></div>
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <?php //if(get_config('local_trainingtroops', 'bac_on')): ?>
            <li>
                <a href="#" data-action="addition">
                    <span class="glyphicon glyphicon-plus-sign">&nbsp;</span>Nouveau bac à sable
                </a>
            </li>
            <?php //endif; ?>
            <li>
                <a href="<?php echo new moodle_url('/local/teacherboard') ?>">
                    <span class="glyphicon glyphicon-blackboard">&nbsp;</span>Accéder à mon espace
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="mailto:support-elea@ac-versailles.fr">
                    <span class="glyphicon glyphicon-envelope">&nbsp;</span>Écrire au support
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="http://www.viaeduc.fr/group/10245" target="_blank">
                    <span class="glyphicon glyphicon-question-sign">&nbsp;</span>Échanger entre pairs
                </a>
            </li>
        </ul>
        <!--<ul class="nav navbar-nav navbar-right">
            <li>
                <a href="<?php echo new moodle_url('/local/faq/index.php?role=formateur') ?>" target="_blank">
                    <span class="glyphicon glyphicon-info-sign">&nbsp;</span>Accéder au tutoriel
                </a>
            </li>
        </ul>-->

    </nav>
</header>



<?php if (count($sandboxes)) : ?>
<div class="row">
    <?php foreach ($sandboxes as $sandbox): ?>
    <div class="bac-col col-sm-4 col-md-3">
        <div class="bac">
            <div class="bac-titre">
                <h2><?php echo $sandbox->name ?></h2>
            </div>
            <div class="bac-info">
                Créé le <?php echo date('d/m/Y',$sandbox->time) ?><br/>
                UAI : <?php echo strtoupper($sandbox->uai) ?><br/>
                <?php echo $sandbox->profs ?> enseignants - <?php echo $sandbox->eleves ?> élèves<br/>
                <a href="resources/troops_siecle.php?uai=<?php echo $sandbox->uai ?>">Fichier XML SIECLE (pour importer des élèves)</a><br/><br/>
            </div>
            <div class="bac-options">
                <a class="tool-tip" href="#" data-action="edition" data-step="resume" data-uai="<?php echo $sandbox->uai ?>">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                    <span class="tooltiptext">Liste des comptes inscrits</span>
                </a>
                <a class="tool-tip" href="#" data-action="edition" data-step="parcours" data-uai="<?php echo $sandbox->uai ?>">
                    <span class="glyphicon glyphicon-education" aria-hidden="true"></span>
                    <span class="tooltiptext">Inscription dans des parcours</span>
                </a>
                <a class="tool-tip" href="#" data-action="edition" data-step="ajout-enseignants-form" data-uai="<?php echo $sandbox->uai ?>">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    <span class="tooltiptext">Ajouter des enseignants</span>
                </a>
                <a class="tool-tip" href="#" data-action="edition" data-step="import-siecle" data-uai="<?php echo $sandbox->uai ?>">
                    <span class="glyphicon glyphicon-paste" aria-hidden="true"></span>
                    <span class="tooltiptext">Importer le fichier d'élèves SIECLE</span>
                </a>
                <a class="tool-tip" href="#" data-action="edition" data-step="mail-form" data-uai="<?php echo $sandbox->uai ?>">
                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                    <span class="tooltiptext">Envoyer un mail aux enseignants</span>
                </a>
                <a class="tool-tip" href="#" data-action="edition" data-step="supprimer-bac" data-uai="<?php echo $sandbox->uai ?>">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    <span class="tooltiptext">Supprimer le bac à sable</span>
                </a>
            </div>
        </div>
    </div>
    <?php endforeach ?>
</div>

<?php else : ?>
<div class="alert alert-warning">Vous n'avez créé aucun bac à sable sur cette plateforme pour l'instant.</div>
<?php endif; ?>

<?php //if(get_config('local_trainingtroops', 'bac_on')): ?>
<div class="form-group text-center">
     <button type="button" class="btn-default" data-action="addition">Ajouter un nouveau bac à sable</button>
</div>
<?php //endif; ?>