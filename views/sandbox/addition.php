<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox addition modal view
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Générer un nouveau bac à sable</h4>
        </div>
        <div class="modal-body">
            
            <div data-step="generer">
                <div class="container-fluid">
                    <form autocomplete="off" class="" id="form-generer">
                        <div class="form-group">
                            <label class="form-control-label" for="form-generer-nom"><h3>Nom du bac à sable</h3></label>
                            <input name="nom" type="text" placeholder="Formation..." required class="form-control" id="form-generer-nom">
                        </div>
                        <div class="form-group">
                            <h3>Enseignants</h3>
                            <label class="form-control-label" for="form-nombre-enseignants">
                                Indiquez le nombre de comptes enseignants que vous voulez générer automatiquement :
                            </label>
                            <input class="form-control" value="0" type="number" step="1" min="0" max="50" id="form-nombre-enseignants">
                            <label class="form-control-label" for="form-generer-mails">
                                Vous pouvez également inscrire des enseignants en renseignant leurs adresses académiques ci-dessous.
                            </label>
                            <textarea rows="3" name="mails" placeholder="Entrez les adresses mail des enseignants à générer dans ce bac à sable, de la forme prenom.nom@ac-versailles.fr;" class="form-control" id="form-generer-mails"></textarea>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn-success" id="form-generer-submit">Générer</button>
                        </div>
                    </form>
                </div>
                <div style="display: none" class="alert alert-danger"></div>
            </div>
            
            <div style="display: none" data-step="reussite">
                <div class="alert alert-success" id="report-success">
                    Le bac à sable a été généré avec succès sur la plateforme&nbsp;!
                </div>
                        <p>
                            Le compte du référent de l'établissement a pour identifiant <strong><span class="form-data-uai"></span></strong>
                            et pour mot de passe <strong><span class="form-data-mdp_referent"></span></strong>.<br/>
                            Si vous voulez tester l'importation d'élèves avec le compte du référent, 
                            vous pouvez <strong><a class="form-siecle" target="_blank" href="resources\troops_siecle.php">télécharger ce fichier SIECLE</a></strong>.
                        </p>
                        <p class="form-data-show-nb_profs">
                            Vous avez créé <strong><span class="form-data-nb_profs"></span> enseignants</strong>.<br/>
                            <span style="color:red">Notez leurs identifiants maintenant : vous ne pourrez pas le faire plus tard !</span><br/>
                            <a id="reussite-print">Cliquez ici pour imprimer ces identifiants.</a>
                            <div id="reussite-table"></div>
                        </p>
                        <p class="form-data-hide-nb_profs">
                            <strong>Vous n'avez pas créé d'enseignant</strong>.<br/>
                            Vous pourrez le faire ultérieurement en sélectionnant ce bac à sable.
                        </p>
            </div>           
        </div>
        <div class="modal-footer">
            <div class="progress">
                <div class="progress-bar" style="width: 50%">Étape 1</div>   
            </div>
        </div>
    </div>
</div>