<?php

// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sandbox addition modal view
 *
 * @package     local_trainingtroops
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?php echo $sandbox->name ?></h4>
        </div>
        <div class="modal-body">
            <input type="hidden" id="form-sandbox-uai" value="<?php echo $sandbox->uai ?>">
            <input type="hidden" id="form-sandbox-name" value="<?php echo $sandbox->name ?>">
            <div data-step="resume">
                <p>Le compte référent de ce bac à sable est <strong><?php echo $sandbox->uai ?></strong><br/>
                    et son mot de passe, s'il n'a pas été modifié, est <strong>referent</strong>
                </p>
                <div>
                    <?php if($sandbox->profs): ?>
                        <strong><?php echo count($sandbox->profs) ?> enseignants sont inscrits</strong> dans ce bac à sable.
                        Le mot de passe des comptes enseignants vous a été donné lors de leur création.<br/>
                        <a href="#" id="afficher-table-enseignants">
                            Afficher les enseignants... 
                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                        </a>
                        <table id="table-enseignants" style="display:none;">
                            <thead>
                                <tr>
                                    <th>Enseignant</th>
                                    <!--
                                    <th>Élève</th>
                                    -->
                                    <!--
                                    <th>Classe</th>
                                    -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($sandbox->profs as $prof): ?>
                                <tr>
                                    <td><?php echo $prof['username']?></td>
                                    <!--
                                    <td>
                                        <?php 
                                            //echo $prof['username_eleve']
                                        ?>
                                    </td>
                                    -->
                                    <!--
                                    <td>
                                        <?php 
                                            //echo $prof['classe']
                                        ?>
                                    </td>
                                    -->
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                       Aucun enseignant n'a été inscrit dans ce bac à sable.
                    <?php endif ?>
                    <?php if(false && $sandbox->nb_eleves_importes):?>
                       <br/><?php if($sandbox->profs) echo 'De plus, '?><strong><?php echo $sandbox->nb_eleves_importes?><?php if($sandbox->profs) echo ' autres'?> élèves ont été importés</strong> dans ce bac à sable.
                    <?php endif ?> 
                </div>
                <br/>
                <div style="display: none" class="alert alert-danger"></div>
            </div>
            <div data-step="parcours">            
                <div>
                    <?php if($sandbox->parcours_formateur): ?>
                    Vous pouvez <strong>inscrire les enseignants de ce bac à sable aux parcours de votre dossier "Formation"</strong>.<br/> 
                    Pour cela, sélectionnez les parcours auxquels vous voulez inscrire les enseignants :
                    <?php foreach($sandbox->parcours_formateur as $parcours): ?>
                        <br/><input type="checkbox" class="parcours-formateur" id="parcours-formateur-<?php echo $parcours->id ?>" value="<?php echo $parcours->id ?>">
                        <label for="parcours-formateur-<?php echo $parcours->id ?>"> <?php echo $parcours->fullname ?></label>
                    <?php endforeach ?>
                    <div class="button-center">
                        <button id="injecter-cours-formateur" class="btn-success">Inscrire les enseignants à ce(s) parcours</button> 
                    </div>
                   <?php else: ?>
                    Vous pouvez <strong>inscrire les enseignants de ce bac à sable aux parcours de votre dossier "Formation"</strong>, mais soit vous n'avez pas de dossier "Formation", soit ce dossier est vide.<br/>
                    <a href="<?php echo new moodle_url('/local/teacherboard') ?>">Accéder à votre espace pour créer ou remplir ce dossier.</a>
                   <?php endif ?>  
                </div>
                <br/>
                <div>
                    <?php if($sandbox->parcours): ?>
                    <strong><?php echo count($sandbox->parcours) ?> parcours ont été créés </strong> dans ce bac à sable.<br/>
                    <a href="#" id="afficher-table-parcours">
                        Afficher les parcours... 
                        <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    </a>
                    <table id="table-parcours" style="display:none;">
                        <thead>
                            <tr>
                                <th>Nom du parcours</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($sandbox->parcours as $parcours): ?>
                            <tr>
                                <td><?php echo $parcours->fullname ?></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <br/>
                    <div class="button-center">
                        <button id="injecter-cours" class="btn-success">S'inscrire dans tous les parcours du bac à sable</button> 
                    </div>
                    <?php else: ?>
                    Aucun enseignant de ce bac à sable n'a créé de parcours pour l'instant.
                    <?php endif ?>
                </div>
                <br/>
                <div style="display: none" class="alert alert-danger"></div>
            </div>
            
            <div style="display: none" data-step="injection">
                <div style="display: none" class="alert alert-danger"></div>
                <p class="form-data-error"></p>
                <p class="form-data-hide-error">
                    Vous êtes maintenant inscrit dans tous les parcours de ce bac à sable.<br/>
                    Vous pouvez voir et éditer chacun de ces parcours.
                </p>
            </div>
            
            <div style="display: none" data-step="injection-formateur">
                <div style="display: none" class="alert alert-danger"></div>
                <p class="form-data-error"></p>
                <p class="form-data-hide-error">
                    Les enseignants sont maintenant inscrits aux parcours que vous avez sélectionnés.<br/>
                    Ils peuvent voir ces parcours.
                </p>
            </div>
            
            <div data-step="ajout-enseignants-form">
                <h3>Ajouter des enseignants</h3>
                <br>
                Créer et ajouter <input value="0" type="number" step="1" min="0" max="50" id="form-nombre-enseignants"> enseignants à ce bac à sable.<br/>
                et/ou ajouter des enseignants avec leurs adresses académiques :
                <textarea rows="3" name="mails" placeholder="Entrez les adresses mail des enseignants à générer dans ce bac à sable, de la forme prenom.nom@ac-versailles.fr;" class="form-control" id="form-generer-mails"></textarea>
                <br/>
                <div class="button-center">
                    <button id="ajouter-enseignants" class="btn-success">Ajouter des enseignants</button> 
                </div>
                <br/>
                <div style="display: none" class="alert alert-danger"></div>
            </div>
            
            <div style="display: none" data-step="ajout-enseignants">
                <div style="display: none" class="alert alert-danger"></div>
                <p class="form-data-error"></p>
                <p class="form-data-hide-error">Vous avez ajouté <strong><span class="form-data-nb_profs"></span></strong> enseignants.<br/>
                <span style="color:red">Notez leurs identifiants maintenant : vous ne pourrez pas le faire plus tard !</span><br/>
                    <a id="ajout-enseignants-print">Cliquez ici pour imprimer ces identifiants.</a></p>
                <div id="ajout-enseignants-table"></div>
            </div>
            
            <div data-step="mail-form">
                <h3>Envoyer un mail aux enseignants</h3>
                Si vous avez inscrit des enseignants grâce à leur adresse académique, vous pouvez leur envoyer un mail en remplissant ce champ.
                <br><br>
                <textarea rows="3" placeholder="Entrez le contenu de votre mail..." class="form-control" id="form-message"></textarea>
                <br/>
                <div class="button-center">
                    <button id="mail-envoyer" class="btn-success">Envoyer un mail</button> 
                </div>
                <br/>
                <div style="display: none" class="alert alert-danger"></div>
            </div>
            
            <div style="display: none" data-step="mail-resultat">
                <div style="display: none" class="alert alert-danger"></div>
                <p class="form-data-error"></p>
            </div>
            
            <div style="display: none" data-step="supprimer-bac">
                <p>Veuillez confirmer que vous voulez bien supprimer ce bac à sable.<br/>
                <strong>Cette opération est irréversible.</strong></p>
                <p>Notez que les professeurs inscrits dans ce bac à sable <strong>garderont accès à leur compte</strong> jusqu'à la fin d'année
                    (pour récupérer les parcours qu'ils auraient éventuellement créés).<br/>
                    En revanche, <strong>les comptes élève seront désactivés</strong>.
                <div class="button-center">
                    <button id="supprimer-bac-ok" class="btn-danger">Supprimer ce bac à sable</button> 
                </div>
                <br/>
                <div style="display: none" class="alert alert-danger"></div>
            </div>
            
            <div style="display: none" data-step="supprimer-resultat">
                <div style="display: none" class="alert alert-danger"></div>
                <p class="form-data-error"></p>
            </div>
            
            <div style="display: none" data-step="import-siecle">
                <p>Veuillez confirmer que vous voulez bien <strong>importer les élèves du fichier XML SIECLE correspondant à ce bac à sable</strong>.</p>
                <div class="button-center">
                    <button id="import-siecle-ok" class="btn-success">Importer les élèves</button> 
                </div>
                <br/>
                <div style="display: none" class="alert alert-danger"></div>
            </div>
            
            <div style="display: none" data-step="import-siecle-resultat">
                <div style="display: none" class="alert alert-danger"></div>
                <p class="form-data-error"></p>
                <div class="form-data-show-newcount">
                    <strong class="form-data-newcount"></strong> nouveaux élèves ont été importés :
                    <ul class="form-data-newhtml"></ul>
                </div>
                <div class="form-data-show-existingcount">
                    <strong class="form-data-existingcount"></strong> élèves avaient déjà été importés sur ce bac à sable :
                    <ul class="form-data-existinghtml"></ul>
                </div>
                <p>Tous ces élèves se connecteront avec le mot de passe <strong>eleve</strong>.
            </div>
           
            
        </div>
    </div>
</div>