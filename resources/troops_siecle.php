<?php
// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * XML Siecle-like file generator
 *
 * @package     local_trainingtroops
 * @author      Charly Piva
 * @copyright   (C) Charly Piva 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../classes/local/models/troops_array.php');


header('Content-Type: text/xml');
header('Content-Type: application/xml');
   
$xml = '<?xml version="1.0" encoding="ISO-8859-15"?>';
$xml .= '<BEE_ELEVES VERSION="1.8">';

$xml .= '<PARAMETRES>';
$xml .= '<UAJ>'.strtoupper($_GET['uai']).'</UAJ>';
$xml .= '<ANNEE_SCOLAIRE>'.date("Y", strtotime("-8 months")).'</ANNEE_SCOLAIRE>';
$xml .= '<DATE_EXPORT>'.date("d/m/Y").'</DATE_EXPORT>';
$xml .= '<HORODATAGE>'.date("d/m/Y H:i:s").'</HORODATAGE>';
$xml .= '</PARAMETRES>';

$xml .= '<DONNEES>';


$troops = (new local_trainingtroops\local\models\troops_array())->getTroops($_GET['uai']);


$xml .= '<ELEVES>';

foreach($troops as $i => $troop){
    //$xml .=  '<ELEVE ELEVE_ID="'.str_pad($i+1,6,'0',STR_PAD_LEFT).substr(strtoupper($_GET['uai']),0,-1).'">';
    $xml .=  '<ELEVE ELEVE_ID="'.$troop['id'].'">';
    $xml .=  '<NOM>'.$troop['nom'].'</NOM>';
    $xml .=  '<PRENOM>'.$troop['prenom'].'</PRENOM>';
    $xml .=  '</ELEVE>';
}

$xml .= '</ELEVES>';

$xml .= '<STRUCTURES>';

foreach($troops as $i => $troop){
    $xml .=  '<STRUCTURES_ELEVE ELEVE_ID="'.$troop['id'].'">';
    $xml .=  '<STRUCTURE>';
    $xml .=  '<CODE_STRUCTURE>'.$troop['classe'].'</CODE_STRUCTURE>';
    $xml .=  '</STRUCTURE>';
    $xml .=  '</STRUCTURES_ELEVE>';
}

$xml .= '</STRUCTURES>';

$xml .=   '</DONNEES></BEE_ELEVES>';

echo $xml;